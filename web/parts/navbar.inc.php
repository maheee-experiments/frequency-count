<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="?">Utility Frequency</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?=$todayMode ? 'active' : ''?>" href="#" data-toggle="dropdown">
          <i class="fas fa-chart-line"></i> Today
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="?h=1">last hour</a>
          <a class="dropdown-item" href="?h=6">last 6 hours</a>
          <a class="dropdown-item" href="?h=12">last 12 hours</a>
          <a class="dropdown-item" href="?h=24">last 24 hours</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?=$pastMode ? 'active' : ''?>" href="#" data-toggle="dropdown">
          <i class="fas fa-calendar-day"></i> Past
        </a>
        <div class="dropdown-menu">
          <?php
            function mapMonth($date) {
              return substr($date, 0, 7);
            }

            $resultDays = getDaysWithData();
            $months = array_unique(array_map("mapMonth", $resultDays));
            foreach ($months as $month) {
              $collapseId = "collapse_" . $month;
              echo "<a class=\"dropdown-item dropdown-toggle no-close\" data-toggle=\"collapse\" data-target=\"" . $collapseId . "\"  href=\"#" . $collapseId . "\">" . $month . "</a>";
              echo "<div class=\"collapse\" id=\"" . $collapseId . "\">";
              echo "<div class=\"dropdown-divider\"></div>";
              foreach ($resultDays as $day) {
                if (mapMonth($day) == $month) {
                  echo "<a class=\"dropdown-item\" href=\"?d=" . $day . "\">" . $day . "</a>";
                }
              }
              echo "<div class=\"dropdown-divider\"></div>";
              echo "</div>";
            }
            echo "\n";
          ?>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link <?=$statMode ? 'active' : ''?>" href="?s">
          <i class="fas fa-chart-line"></i> Statistics
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?=$methMode ? 'active' : ''?>" href="?m">
          <i class="fas fa-flask"></i> Methods
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://mahes.net/pages/imprint.php" target="_blank">
          <i class="fas fa-external-link-alt"></i> Imprint
        </a>
      </li>
    </ul>
  </div>
</nav>
<script>
$('.no-close').on("click", function (e) {
  $("#" + e.target.dataset.target).collapse('toggle');
  e.stopPropagation();
  e.preventDefault();
});
</script>
