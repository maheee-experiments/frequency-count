<?php
  $hours = (int) $_GET["h"];
  if ($hours > 24) {
    $hours = 24;
  }
  if ($hours < 1) {
    $hours = 1;
  }
  $dataPointArray = getLastHours($hours);
  $headerText = 'last ' . $hours . ' hours';
?>
<div class="card-deck">
  <?=createGraph('main', createDefaultGraphOptions($dataPointArray), 'Frequency - ' . $headerText)?>
</div>
