#ifndef _CDCACM_CDCACM_H_
#define _CDCACM_CDCACM_H_

#include <libopencm3/usb/usbd.h>

extern "C"
{
  usbd_device *usb_cdcacm_init();
}

void usb_cdcacm_print(usbd_device *usbd_dev, const char *s);

#endif
