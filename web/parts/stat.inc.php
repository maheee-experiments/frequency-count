<?php
  $dataPointArrays = getMinMaxAvg();
?>
<div class="card-deck">
  <?=createGraph('main', createMinMaxAvgGraphOptions($dataPointArrays), 'Min, Max, Avg per day', false)?>
</div>
