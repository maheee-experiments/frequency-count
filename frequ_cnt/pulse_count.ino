volatile unsigned long pulseLengthsResult = 0;
volatile int pulseLengthsResultCount = 0;

void pulse() {
  static unsigned long lastMicros = ULONG_LONG_MAX;
  static int pulseCount = 0;
  static unsigned long pulseLengths = 0;

  unsigned long now = micros();
  long diff = now - lastMicros;

  if (diff > 10000) { // catch overflows as well as flicker
    pulseLengths += now - lastMicros;
    ++pulseCount;
    lastMicros = now;
  } else if (diff <= 0) { // ignore flicker, fix overflow
    lastMicros = now;
  }

  if (pulseCount >= PULSES_TO_COUNT) {
    pulseLengthsResult = pulseLengths;
    ++pulseLengthsResultCount;
    pulseCount = 0;
    pulseLengths = 0;
  }
}

unsigned long getNewPulse() {
  static int lastPulseLengthsResultCount = pulseLengthsResultCount;

  if (lastPulseLengthsResultCount != pulseLengthsResultCount) {
    unsigned long pulseLengths = pulseLengthsResult;
    lastPulseLengthsResultCount = pulseLengthsResultCount;
    return pulseLengths;
  }

  return 0;
}
