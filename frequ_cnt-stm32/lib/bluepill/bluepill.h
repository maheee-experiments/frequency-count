#ifndef _BLUEPILL_BLUEPILL_H_
#define _BLUEPILL_BLUEPILL_H_

#include <libopencm3/stm32/spi.h>    //  For SPI port definitions e.g. SPI1
#include <libopencm3/stm32/i2c.h>    //  For I2C port definitions e.g. I2C1
#include <libopencm3/stm32/usart.h>  //  For USART port definitions e.g. USART1
#include <stdint.h>                  //  For uint32_t


void rtc_setup();
uint32_t millis(void);

#endif
