#ifndef _SETUP_H_
#define _SETUP_H_

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>

#include <cdcacm.h>
#include "stringout.h"


void io_setup();
void systick_setup();
usbd_device* output_setup();

#endif
