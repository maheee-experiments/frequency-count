<?php

function createStripLineOption($startValue, $endValue, $color, $label) {
  return array(
    "startValue" => $startValue,
    "endValue" => $endValue,
    "color" => $color,
    "label" => $label,
    "labelFontColor" => "#00000050",
    "labelBackgroundColor" => "transparent"
  );
}

function createDefaultGraphOptions($dataPointArray) {
  $arr = array(
    "animationEnabled" => true,
    "zoomEnabled" => true,
    "zoomType" => "xy",
    "axisX" => array(
      "labelFontSize" => 10,
      "valueFormatString" => "DD.MM. HH:mm",
      "gridThickness" => 1,
      "gridColor" => "#00000010"
    ),
    "axisY" => array(
      "suffix" => " Hz",
      "prefix" => "",
      "minimum" => 49.8,
      "maximum" => 50.2,
      "labelFontSize" => 10,
      "stripLines" => array(
        createStripLineOption(0, 47.5, "#FF4444", "blackout"),
        createStripLineOption(47.5, 48.1, "#FF6666", "load shedding 50-70%"),
        createStripLineOption(48.1, 48.4, "#FF8888", "load shedding 35-50%"),
        createStripLineOption(48.4, 48.7, "#FFAAAA", "load shedding 20-30%"),
        createStripLineOption(48.7, 49.0, "#FFCCCC", "load shedding 10-15%"),
        createStripLineOption(49.0, 49.8, "#FFEEEE", ""),
        //createStripLineOption(49.8, 50.2, "#EEFFEE", ""),
        createStripLineOption(50.2, 51.5, "#FFEEEE", "shutdown of solar power"),
        createStripLineOption(51.5, 100, "#FFCCCC", "")
      )
    ),
    "data" => array(
      array(
        "type" => "spline",
        "markerSize" => 0,
        "xValueType" => "dateTime",
        "xValueFormatString" => "DD.MM. HH:mm:ss",
        "dataPoints" => $dataPointArray
      )
    )
  );
  return trim(preg_replace('/\s+/', ' ', json_encode($arr, JSON_NUMERIC_CHECK)));
}

function createMinMaxAvgGraphOptions($dataPointArrays) {
  $arr = array(
    "animationEnabled" => true,
    "zoomEnabled" => false,
    "axisX" => array(
      "labelFontSize" => 10,
      "gridThickness" => 1,
      "gridColor" => "#00000010"
    ),
    "axisY" => array(
      "suffix" => " Hz",
      "prefix" => "",
      "minimum" => 49.8,
      "maximum" => 50.2,
      "labelFontSize" => 10,
    ),
    "data" => array(
      array(
        "type" => "rangeArea",
        "markerSize" => 0,
        "xValueType" => "dateTime",
        "xValueFormatString" => "DD.MM.YYYY",
        "dataPoints" => $dataPointArrays[0]
      ),
      array(
        "type" => "spline",
        "markerSize" => 0,
        "xValueType" => "dateTime",
        "xValueFormatString" => "DD.MM.YYYY",
        "dataPoints" => $dataPointArrays[1]
      )
    )
  );
  return trim(preg_replace('/\s+/', ' ', json_encode($arr, JSON_NUMERIC_CHECK)));
}

function createDefaultGaugeOptions($value) {
  $arr = array(
    "animationEnabled" => true,
    "title" => array(
      "fontColor" => "#848484",
      "fontSize" => 50,
      "horizontalAlign" => "center",
      "text" => round($value, 2) . " Hz",
      "verticalAlign" => "center"
    ),
    "data" => array(
      array(
        "explodeOnClick" => false,
        "innerRadius" => "80%",
        "radius" => "100%",
        "startAngle" => 90,
        "type" => "doughnut",
        "dataPoints" => array(
          array("y" => 0.01, "color" => "#ffffff", "toolTipContent" => null, "highlightEnabled" => false),
          array("y" => ($value < 50 ? 0.2 - abs($value - 50) : 0.2), "color" => "#cccccc", "toolTipContent" => null, "highlightEnabled" => false),
          array("y" => abs($value - 50), "color" => "#4661EE", "toolTipContent" => (round($value, 3) . " Hz")),
          array("y" => ($value > 50 ? 0.2 - abs($value - 50) : 0.2), "color" => "#cccccc", "toolTipContent" => null, "highlightEnabled" => false),
          array("y" => 0.01, "color" => "#ffffff", "toolTipContent" => null, "highlightEnabled" => false)
        )
      )
    )
  );
  return trim(preg_replace('/\s+/', ' ', json_encode($arr, JSON_NUMERIC_CHECK)));
}

?>
