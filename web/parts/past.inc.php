<?php
  $dataPointArray = getDay($_GET["d"]);
  $headerText = $_GET["d"];
?>
<div class="card-deck">
  <?=createGraph('main', createDefaultGraphOptions($dataPointArray), 'Frequency - ' . $headerText)?>
</div>
