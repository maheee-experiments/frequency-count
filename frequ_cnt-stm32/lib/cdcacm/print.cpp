#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/cdc.h>
#include <string.h>
#include "cdcacm.h"

void usb_cdcacm_print(usbd_device *usbd_dev, const char *s)
{
  usbd_ep_write_packet(usbd_dev, 0x82, s, strlen(s));
}
