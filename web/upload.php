<?php
error_reporting(E_ERROR);
include "./db/db.inc.php";


$pulseLengthSum = (int) $_GET["pls"];
$pulses = (int) $_GET["p"];
$frequency = (1000000.0 * $pulses) / $pulseLengthSum;

$mysqli = connectDb();

$stmt = $mysqli->prepare("INSERT INTO measurements (pulseLengthSum, pulses, frequency) VALUES (?, ?, ?)");
if (!$stmt) {
  header("X-Error-Message: Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error, true, 500);
  exit();
}

$stmt->bind_param("iid", $pulseLengthSum, $pulses, $frequency);

if (!$stmt->execute()) {
  header("X-Error-Message: Execute failed: (" . $stmt->errno . ") " . $stmt->error, true, 500);
  exit();
}

echo $frequency;

disconnectDb($mysqli);

?>
