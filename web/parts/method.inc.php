<style>
.modal-body > img {
  max-width: 100%;
  max-height: 100%;
  cursor: pointer;
}
.media > img {
  cursor: pointer;
}
</style>

<?php
function imgMediaObj($id, $thumb, $img, $body) {
?>
  <div class="col-lg">
    <div class="media">
      <img src="<?=$thumb?>" class="mr-3 img-thumbnail" width="128" data-toggle="modal" data-target="#<?=$id?>">
      <div class="media-body">
        <?=$body?>
      </div>
    </div>
    <div class="modal fade" id="<?=$id?>" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <img src="<?=$img?>" data-toggle="modal" data-target="#<?=$id?>">
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
}
?>
<div class="card-deck">
  <div class="card">
    <div class="card-header">
      Hardware
    </div>
    <div class="card-body">

      <div class="container">
        <div class="row">
          <?=imgMediaObj('i0', './img/t_0_schematic.jpg', './img/0_schematic.jpg', 'The schematic looks something like this. The blocky thing on the right is an ESP8266. It has it\'s own power supply which I forgot to draw here.');?>
        </div>

        <div class="row"><div class="col"><hr/></div></div>

        <div class="row">
          <?=imgMediaObj('i1', './img/t_1_test_setup_1.jpg', './img/1_test_setup_1.jpg', 'A first test setup on a breadboard ...');?>
          <?=imgMediaObj('i2', './img/t_2_test_setup_2.jpg', './img/2_test_setup_2.jpg', '... and with power supply.');?>
          <?=imgMediaObj('i3', './img/t_3_test_setup_3.jpg', './img/3_test_setup_3.jpg', 'And finally everything soldered together.');?>
        </div>

        <div class="row"><div class="col"><hr/></div></div>

        <div class="row">
          <?=imgMediaObj('i4', './img/t_4_box_1.jpg', './img/4_box_1.jpg', 'Glued on the bottom of a 3D printed box, ...');?>
          <?=imgMediaObj('i5', './img/t_5_box_2.jpg', './img/5_box_2.jpg', '... made of transparent PLA.');?>
          <?=imgMediaObj('i6', './img/t_6_box_3.jpg', './img/6_box_3.jpg', 'The box put together, glowing in the dark.');?>
        </div>

        <div class="row"><div class="col"><hr/></div></div>

        <div class="row">
          <?=imgMediaObj('i7', './img/t_7_final.jpg', './img/7_final.jpg', 'And working at it\'s final destination.');?>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="card-deck">
  <div class="card">
    <div class="card-header">
      Software
    </div>
    <div class="card-body">
      <p>Details coming soon ...</p>
      <p>
        In short:
        <ul>
          <li>Optocoupler pulls pin down 50 times a seconds.</li>
          <li>Raising flank triggers interrupt.</li>
          <li>Measure passed microseconds between interrupts.</li>
          <li>Do this 100 times.</li>
          <li>Calculate average.</li>
          <li>Send value to backend.</li>
          <li>Profit! (well, not really)</li>
        </ul>
      </p>
    </div>
  </div>
</div>
