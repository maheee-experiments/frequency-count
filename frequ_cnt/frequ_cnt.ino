#include "ESP8266WiFi.h"
#include <ESP8266WiFi.h>

#include "a_config.h"
#include "config.h"


void setup() {
  pinMode(PULSE_PIN, INPUT_PULLUP);

  pinMode(LED_WLAN,    OUTPUT);
  pinMode(LED_FREQ_M2, OUTPUT);
  pinMode(LED_FREQ_M1, OUTPUT);
  pinMode(LED_FREQ_OK, OUTPUT);
  pinMode(LED_FREQ_P1, OUTPUT);
  pinMode(LED_FREQ_P2, OUTPUT);

  Serial.begin(57600);

  initWifi(SSID, PASSWORD);

  attachInterrupt(digitalPinToInterrupt(PULSE_PIN), pulse, RISING);
}


void loop() {
  unsigned long pulseLengths = getNewPulse();
  if (pulseLengths > 0) {
    unsigned long frequency = PULSE_DIVIDEND / pulseLengths;

    if (sendData(HOST, PORT, pulseLengths)) {
      digitalWrite(LED_WLAN, HIGH);
    } else {
      digitalWrite(LED_WLAN, LOW);
    }

    Serial.println(frequency);
    ledFun(frequency);
  }
}


void ledFun(unsigned long frequency) {
  digitalWrite(LED_FREQ_M2, LOW);
  digitalWrite(LED_FREQ_M1, LOW);
  digitalWrite(LED_FREQ_OK, LOW);
  digitalWrite(LED_FREQ_P1, LOW);
  digitalWrite(LED_FREQ_P2, LOW);

  if (frequency > 50150) {
    digitalWrite(LED_FREQ_P2, HIGH);
  } else if (frequency > 50050) {
    digitalWrite(LED_FREQ_P1, HIGH);
  } else if (frequency < 49850) {
    digitalWrite(LED_FREQ_M2, HIGH);
  } else if (frequency < 49950) {
    digitalWrite(LED_FREQ_M1, HIGH);
  } else {
    digitalWrite(LED_FREQ_OK, HIGH);
  }
}

