
//#define DEBUG // if defined, output happens via debugging port
#define USB // if defined, output happens via usb

#define MEASURE_CNT 50
#define HALF_MEASURE_CNT MEASURE_CNT / 2
#define MAX_TICK_COUNT 40000 // 20000 would be about 50Hz; double that is clearly wrong
#define MIN_TICK_COUNT 10000 // 20000 would be about 50Hz; half that is clearly wrong