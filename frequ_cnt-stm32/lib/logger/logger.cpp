#include "logger.h"
#include <string.h>

void debug_print(const char *s)
{
  if (s[0] != 0)
  {
    semihost_write((const unsigned char *)s, strlen(s));
  }
}
