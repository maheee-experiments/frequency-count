<?php

function getFrequencySanitizationCondition() {
  return "frequency > 40 AND frequency < 60";
}

function getBaseQuery() {
  return "SELECT timestamp, frequency FROM measurements WHERE " . getFrequencySanitizationCondition();
}

function getOrderTerm() {
  return "ORDER BY timestamp ASC";
}

function queryDaysWithData() {
  global $mysqli;

  $stmt = $mysqli->prepare("SELECT distinct date(timestamp) as day FROM measurements ORDER BY timestamp DESC");

  $stmt->execute();
  return $stmt->get_result();
}

function queryLastHours($hours) {
  global $mysqli;

  $stmt = $mysqli->prepare(
    getBaseQuery() .
    " AND timestamp > (now() - interval ? hour) " .
    getOrderTerm());

  $stmt->bind_param("i", $hours);
  $stmt->execute();
  return $stmt->get_result();
}

function queryDay($day) {
  global $mysqli;

  $stmt = $mysqli->prepare(
    getBaseQuery() .
    " AND timestamp > date_add(cast(? as datetime), interval -1 hour) AND timestamp < date_add(cast(? as datetime), interval 25 hour) " .
    getOrderTerm());

  $stmt->bind_param("ss", $day, $day);
  $stmt->execute();
  return $stmt->get_result();
}

function queryMinMaxAvg() {
  global $mysqli;

  $stmt = $mysqli->prepare("SELECT timestamp(date(timestamp)) as timestamp, min(frequency) as min, max(frequency) as max, avg(frequency) as avg FROM measurements GROUP BY date(timestamp)");

  $stmt->execute();
  return $stmt->get_result();
}

function checkQueryResultOrDie($result) {
  global $mysqli;

  if (!$result) {
    header("X-Error-Message: Query: " . $mysqli->error, true, 500);
    exit();
  }
}

?>
