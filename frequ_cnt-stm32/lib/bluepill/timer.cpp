#include <string.h>
#include <libopencm3/cm3/cortex.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/rtc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/nvic.h>
#include "bluepill.h"


static volatile uint32_t milliTickCount = 0; // Number of millisecond ticks elapsed.

uint32_t millis(void)
{
  return milliTickCount;
}

void rtc_isr(void)
{
  if (rtc_check_flag(RTC_SEC))
  {
    rtc_clear_flag(RTC_SEC);
    milliTickCount += 10;

    return;
  }
}

void rtc_setup(void)
{
  rcc_enable_rtc_clock();
  rtc_interrupt_disable(RTC_SEC);
  rtc_interrupt_disable(RTC_ALR);
  rtc_interrupt_disable(RTC_OW);

  // RCC_HSE: 62.5 kHz, fastest oscillator, doesn't work in Stop or Standby Low Power mode. 
  // RCC_LSE: 32.768 kHz, slowest oscillator, works in Stop or Standby Low Power mode. 
  // RCC_LSI: 40 kHz, works in Stop or Standby Low Power mode. 
  rtc_awake_from_off(HSE);

  rtc_set_prescale_val(625); // 10 ms ticks

  nvic_enable_irq(NVIC_RTC_IRQ);

  cm_disable_interrupts();
  rtc_clear_flag(RTC_SEC);
  rtc_clear_flag(RTC_ALR);
  rtc_clear_flag(RTC_OW);
  rtc_interrupt_enable(RTC_SEC);
  cm_enable_interrupts();
}
