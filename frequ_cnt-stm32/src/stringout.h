#ifndef _PRINT_H_
#define _PRINT_H_

#include <libopencm3/usb/usbd.h>

void enable_debug();
void enable_usb_cdcacm(usbd_device *usbd_dev);

void printout(const char *s);

#endif
