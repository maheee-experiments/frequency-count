
void initWifi(char* ssid, char* password) {
  Serial.print("InitWifi: Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  Serial.println("InitWifi: WiFi connected");
  Serial.print("InitWifi: IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("");
}

boolean sendData(char* host, int port, int pulseLengths) {
  WiFiClient client;
  if (!client.connect(host, port)) {
    Serial.println("SendData: Connection Failed!");
    return false;
  }

  String url = String("/upload.php?pls=") + pulseLengths + "&p=" + PULSES_TO_COUNT;
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > WIFI_CLIENT_TIMEOUT) {
      Serial.println("SendData: Client Timeout!");
      client.stop();
      return false;
    }
  }

  while (client.available()) {
    // String line = client.readStringUntil('\r');
    // Serial.print(line);
    client.readStringUntil('\r');
  }

  client.stop();

  return true;
}
