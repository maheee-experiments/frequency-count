<?php

function getCacheItem($path) {
  global $pool;

  return $pool->getItem($path);
}

function saveCacheItem($item, $data, $expiresAfter) {
  global $pool;

  $item->set($data);
  $item->setInvalidationMethod(Stash\Invalidation::OLD);
  $item->expiresAfter($expiresAfter);
  $pool->save($item);
}

/*
 *
 */
function getDaysWithData() {
  $item = getCacheItem('getdayswithdata');
  $data = $item->get();

  if($item->isMiss()) {
    $item->lock();

    $result = queryDaysWithData();
    checkQueryResultOrDie($result);
    $data = createDayList($result);

    saveCacheItem($item, $data, 3600); // 1 h
  }

  return $data;
}

function getLastHours($hours) {
  $item = getCacheItem('getlasthours/' . $hours);
  $data = $item->get();

  if($item->isMiss()) {
    $item->lock();

    $result = queryLastHours($hours);
    checkQueryResultOrDie($result);
    $data = createDatapointArray($result);

    saveCacheItem($item, $data, $hours == 1 ? 30 : 300); // 30 sec or 5 min
  }

  return $data;
}

function getDay($day) {
  $item = getCacheItem('getday/' . $day);
  $data = $item->get();

  if($item->isMiss()) {
    $item->lock();

    $result = queryDay($day);
    checkQueryResultOrDie($result);
    $data = createDatapointArray($result);

    saveCacheItem($item, $data, 3600); // 1 h
  }

  return $data;
}

function getMinMaxAvg() {
  $item = getCacheItem('getminmaxavg');
  $data = $item->get();

  if($item->isMiss()) {
    $item->lock();

    $result = queryMinMaxAvg();
    checkQueryResultOrDie($result);
    $data = createMinMaxAvgDatapointArrays($result);

    saveCacheItem($item, $data, 86400); // 1 d
  }

  return $data;
}

/*
 *
 */
function createDayList($result) {
  $days = array();
  if ($result->num_rows > 0) {
    while($row = $result->fetch_array(MYSQLI_NUM)) {
      array_push($days, $row[0]);
    }
  }
  return $days;
}

function createDatapointArray($result) {
  $dataPoints = array();
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      array_push($dataPoints, array("x"=> strtotime($row["timestamp"])*1000, "y"=> $row["frequency"]));
    }
  }
  return $dataPoints;
}

function createMinMaxAvgDatapointArrays($result) {
  $minMaxDataPoints = array();
  $avgDataPoints = array();
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      array_push($minMaxDataPoints, array("x"=> strtotime($row["timestamp"])*1000, "y"=> array($row["min"], $row["max"])));
      array_push($avgDataPoints, array("x"=> strtotime($row["timestamp"])*1000, "y"=> $row["avg"]));
    }
  }
  return array($minMaxDataPoints, $avgDataPoints);
}
?>
