<?php
  $dataPointArray = getLastHours(1);
?>
<div class="card-deck">
  <div class="card">
    <div class="card-header">
      Information
    </div>
    <div class="card-body">
      <p class="mb-0">This page shows the utility frequency of the power net measured in <a href="https://goo.gl/maps/WE5SHBquLVP2" target="_blank"><i class="fas fa-map-marked"></i> Linz, Austria</a> on an ordinary household power outlet.</p>
      <p class="mb-0">For details about the method and hardware see <a href="?m"><i class="fas fa-flask"></i> Method</a>.</p>
    </div>
  </div>
</div>
<div class="card-deck">
  <?=createGauge('mainGauge', createDefaultGaugeOptions(end($dataPointArray)["y"]), 'Frequency - ' . date("Y-m-d, H:i:s", end($dataPointArray)["x"]/1000), 300)?>
  <div class="w-100 d-none d-xs-block d-sm-block d-md-block d-lg-none"></div>
  <?=createGraph('main', createDefaultGraphOptions($dataPointArray), 'Frequency - last hour', false, 300)?>
</div>
