
#define D00  16
#define D01   5
#define D02   4
#define D03   0
#define D04   2
#define D05  14
#define D06  12
#define D07  13
#define D08  15
#define D09   3
#define D10   1


#define SSID     A_SSID
#define PASSWORD A_PASSWORD
#define HOST     A_HOST
#define PORT     A_PORT
#define WIFI_CLIENT_TIMEOUT 1000


#define PULSE_PIN       D01
#define PULSES_TO_COUNT 100
#define PULSE_DIVIDEND  100000000000UL


#define LED_WLAN     D00
#define LED_FREQ_P2  D02
#define LED_FREQ_P1  D04
#define LED_FREQ_OK  D05
#define LED_FREQ_M1  D06
#define LED_FREQ_M2  D07

