--
-- Table structure for table `measurements`
--
CREATE TABLE IF NOT EXISTS `measurements` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pulseLengthSum` int(10) unsigned NOT NULL,
  `pulses` int(10) unsigned NOT NULL,
  `frequency` decimal(8,6) NOT NULL,
  PRIMARY KEY (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
