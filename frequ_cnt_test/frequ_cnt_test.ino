#include "config.h"
#include <limits.h>


void setup() {
  pinMode(PULSE_PIN, INPUT_PULLUP);

  pinMode(BUTTON_A_PIN, INPUT_PULLUP);
  pinMode(BUTTON_B_PIN, INPUT_PULLUP);
  pinMode(BUTTON_C_PIN, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(PULSE_PIN), pulse, RISING);

  Serial.begin(9600);
}

bool first = true;

void loop() {
  unsigned long pulseLengths = getNewPulse();
  if (pulseLengths > 0) {
    unsigned long frequency = PULSE_DIVIDEND / pulseLengths;

    /*
    Serial.print(digitalRead(BUTTON_A_PIN));
    Serial.print(' ');
    Serial.print(digitalRead(BUTTON_B_PIN));
    Serial.print(' ');
    Serial.print(digitalRead(BUTTON_C_PIN));
    Serial.print(' ');
    */

    if (first) {
      first = false;
    } else {
      Serial.println(frequency);
    }
  }

}
