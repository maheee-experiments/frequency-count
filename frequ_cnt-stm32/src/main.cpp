#include <bluepill.h>
#include <logger.h>

#include "config.h"
#include "stringout.h"
#include "setup.h"

volatile uint_fast32_t microTickCountM1 = 0;
volatile uint_fast32_t microTickCountM2 = 0;

volatile uint_fast8_t resultCnt1 = 0;
volatile uint_fast8_t resultCnt2 = 0;
volatile uint_fast32_t result1 = 0;
volatile uint_fast32_t result2 = 0;

volatile uint_fast8_t quality1 = 0;
volatile uint_fast8_t quality2 = 0;

char out_buffer[17];

void sys_tick_handler(void)
{
  // We call this handler every 3us
  microTickCountM1 += 3;
  microTickCountM2 += 3;
}

void exti15_10_isr(void)
{
  static uint_fast16_t measureCntM1 = 0;
  static uint_fast16_t measureCntM2 = HALF_MEASURE_CNT;
  static uint_fast32_t measureSumM1 = 0;
  static uint_fast32_t measureSumM2 = 0;

  static uint_fast32_t m;

  // Optocouplers
  if (exti_get_flag_status(EXTI15))
  {
    exti_reset_request(EXTI15);

    m = microTickCountM1;
    microTickCountM1 = 0;

    if (m > MIN_TICK_COUNT)
    {
      if (m > MAX_TICK_COUNT)
      {
        quality1 = 0;
      }
      measureSumM1 += m;
      ++measureCntM1;
    }

    // check if required amount of measurements is done
    if (measureCntM1 >= MEASURE_CNT)
    {
      result1 = measureSumM1;
      ++resultCnt1;
      measureCntM1 = 0;
      measureSumM1 = 0;

      // check if other channel is half way off
      if ((measureCntM2 < (HALF_MEASURE_CNT - 5)) || (measureCntM2 > (HALF_MEASURE_CNT + 5))) {
        microTickCountM2 = 0;
        measureCntM2 = HALF_MEASURE_CNT;
        measureSumM2 = 0;
        quality2 = 1;
      }
    }
  }
  if (exti_get_flag_status(EXTI14))
  {
    exti_reset_request(EXTI14);

    m = microTickCountM2;
    microTickCountM2 = 0;

    if (m > MIN_TICK_COUNT)
    {
      if (m > MAX_TICK_COUNT)
      {
        quality2 = 0;
      }
      measureSumM2 += m;
      ++measureCntM2;
    }

    // check if required amount of measurements is done
    if (measureCntM2 >= MEASURE_CNT)
    {
      result2 = measureSumM2;
      ++resultCnt2;
      measureCntM2 = 0;
      measureSumM2 = 0;

      // check if other channel is half way off
      if ((measureCntM1 < (HALF_MEASURE_CNT - 5)) || (measureCntM1 > (HALF_MEASURE_CNT + 5))) {
        microTickCountM1 = 0;
        measureCntM1 = HALF_MEASURE_CNT;
        measureSumM1 = 0;
        quality1 = 1;
      }
    }
  }

  // Buttons
  if (exti_get_flag_status(EXTI11))
  {
    exti_reset_request(EXTI11);
    gpio_toggle(GPIOC, GPIO13);
  }
  if (exti_get_flag_status(EXTI12))
  {
    exti_reset_request(EXTI12);
    gpio_toggle(GPIOC, GPIO13);
  }
  if (exti_get_flag_status(EXTI13))
  {
    exti_reset_request(EXTI13);
    gpio_toggle(GPIOC, GPIO13);
  }
}

void print_measurement(uint_fast8_t channel, uint_fast8_t quality, uint_fast16_t cnt, uint_fast32_t sum)
{
  out_buffer[16] = 0;
  out_buffer[15] = '\n';
  out_buffer[14] = '0' + (sum % 10); sum /= 10;
  out_buffer[13] = '0' + (sum % 10); sum /= 10;
  out_buffer[12] = '0' + (sum % 10); sum /= 10;
  out_buffer[11] = '0' + (sum % 10); sum /= 10;
  out_buffer[10] = '0' + (sum % 10); sum /= 10;
  out_buffer[ 9] = '0' + (sum % 10); sum /= 10;
  out_buffer[ 8] = '0' + (sum % 10);
  out_buffer[ 7] = ' ';
  out_buffer[ 6] = '0' + (cnt % 10); cnt /= 10;
  out_buffer[ 5] = '0' + (cnt % 10); cnt /= 10;
  out_buffer[ 4] = '0' + (cnt % 10);
  out_buffer[ 3] = ' ';
  out_buffer[ 2] = '0' + quality;
  out_buffer[ 1] = ' ';
  out_buffer[ 0] = '0' + channel;

  printout(out_buffer);
}

int main(void)
{
  rcc_clock_setup_in_hse_8mhz_out_72mhz();

  io_setup();
  rtc_setup();
  systick_setup();
  usbd_device *usbd_dev = output_setup();

  uint_fast32_t lastResultCnt1 = 0;
  uint_fast32_t lastResultCnt2 = 0;

  while (1)
  {
#ifdef USB
    usbd_poll(usbd_dev);
#endif

    if (resultCnt1 != lastResultCnt1)
    {
      lastResultCnt1 = resultCnt1;

      print_measurement(1, quality1, MEASURE_CNT, result1);
      quality1 = 9;
    }
    if (resultCnt2 != lastResultCnt2)
    {
      lastResultCnt2 = resultCnt2;

      print_measurement(2, quality2, MEASURE_CNT, result2);
      quality2 = 9;
    }
  }

  return 0;
}
