#include <cdcacm.h>
#include <logger.h>
#include "stringout.h"

static bool debug_enabled = false;
static bool usb_cdcacm_enabled = false;
static usbd_device *usbd_dev;

void enable_debug()
{
  debug_enabled = true;
}

void enable_usb_cdcacm(usbd_device *dev)
{
  usbd_dev = dev;
  usb_cdcacm_enabled = true;
}

void printout(const char *s)
{
  if (usb_cdcacm_enabled) {
    usb_cdcacm_print(usbd_dev, s);
    usbd_poll(usbd_dev);
  }
  if (debug_enabled) {
    debug_print(s);
  }
}
