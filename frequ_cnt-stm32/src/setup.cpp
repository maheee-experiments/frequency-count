#include "setup.h"
#include "config.h"


void io_setup()
{
  // onboard LED
  rcc_periph_clock_enable(RCC_GPIOC);
  gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);

  // digital inputs
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_AFIO);
  gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO11 | GPIO12 | GPIO13 | GPIO14 | GPIO15);

  // Configure the EXTI subsystem
  exti_select_source(EXTI11, GPIOB);
  exti_select_source(EXTI12, GPIOB);
  exti_select_source(EXTI13, GPIOB);
  exti_select_source(EXTI14, GPIOB);
  exti_select_source(EXTI15, GPIOB);
  exti_set_trigger(EXTI11 | EXTI12 | EXTI13 | EXTI14 | EXTI15, EXTI_TRIGGER_FALLING);
  exti_enable_request(EXTI11 | EXTI12 | EXTI13 | EXTI14 | EXTI15);
  nvic_enable_irq(NVIC_EXTI15_10_IRQ);

  // put sysclock on pin A8
  //rcc_periph_clock_enable(RCC_GPIOA);
  //gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO8);
  //rcc_set_mco(RCC_CFGR_MCO_HSECLK);
}

void systick_setup()
{
  systick_set_clocksource(STK_CSR_CLKSOURCE_AHB); // 72MHz => 72.000.000 counts per second
  //systick_set_reload(71);                         // SysTick interrupt every 72 clock pulses => every 1 us
  //systick_set_reload(143);                         // SysTick interrupt every 144 clock pulses => every 2 us
  systick_set_reload(215);                        // SysTick interrupt every 216 clock pulses => every 2 us
  systick_clear();
  systick_interrupt_enable();
  systick_counter_enable();
}

usbd_device* output_setup()
{
#ifdef DEBUG
  enable_debug();
#endif
#ifdef USB
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_USB);
  usbd_device* usbd_dev = usb_cdcacm_init();
  enable_usb_cdcacm(usbd_dev);
  usbd_poll(usbd_dev);
  return usbd_dev;
#endif
  return 0;
}
