<?php

function wrapInCard($header, $body) {
  return <<<EOT
  <div class="card">
    <div class="card-header">
      $header
    </div>
    <div class="card-body">
      $body
    </div>
  </div>
EOT;
}


function createGraph($id, $graphOptions, $headerText, $showRangeButtons=true, $height=500) {
  $extraClasses = $showRangeButtons ? '' : 'd-none';
  $height = $height . 'px';

  $script = <<<EOT
    <script>
      $(function () {
        $("#chartContainer_$id").CanvasJSChart($graphOptions);
        $("#button_zoomIn_$id").click(function () {
          $("#chartContainer_$id").CanvasJSChart().options.axisY.minimum = 49.8;
          $("#chartContainer_$id").CanvasJSChart().options.axisY.maximum = 50.2;
          $("#chartContainer_$id").CanvasJSChart().render();
        });
        $("#button_zoomOut_$id").click(function () {
          $("#chartContainer_$id").CanvasJSChart().options.axisY.minimum = 47;
          $("#chartContainer_$id").CanvasJSChart().options.axisY.maximum = 53;
          $("#chartContainer_$id").CanvasJSChart().render();
        });
      });
    </script>
EOT;
  $header = <<<EOT
    $headerText
    <button class="btn btn-outline-secondary btn-sm float-right $extraClasses" id="button_zoomIn_$id">
      <i class="fas fa-search-plus"></i> safe range
    </button>
    <button class="btn btn-outline-secondary btn-sm float-right $extraClasses" id="button_zoomOut_$id">
      <i class="fas fa-search-minus"></i> full range
    </button>
EOT;
  $body = <<<EOT
    <div id="chartContainer_$id" style="height: $height; width: 100%;"></div>
EOT;
  return $script . wrapInCard($header, $body);
}


function createGauge($id, $graphOptions, $headerText, $height=500) {
  $height = $height . 'px';

  $script = <<<EOT
  <script>
    $(function () {
      $("#chartContainer_$id").CanvasJSChart($graphOptions);
    });
  </script>
EOT;

  $body = <<<EOT
    <div id="chartContainer_$id" style="height: $height; width: 100%;"></div>
EOT;
  return $script . wrapInCard($headerText, $body);
}

?>
