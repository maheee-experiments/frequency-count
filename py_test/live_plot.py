import serial

import time

import matplotlib
import matplotlib.pyplot as plt
import numpy as np



t = []
s = []

fig, ax = plt.subplots()
ax.plot(t, s)

ax.set(xlabel='time (s)', ylabel='Hz', title='Frequency')
ax.grid()



def read_freq(ser):
    parts = ser.readline().split()
    return parts
    
with serial.Serial('COM9') as ser:
    while True:
        parts = read_freq(ser)
        freq = int(parts[2]) * 1000000 / int(parts[3])
        print(parts[0], parts[1], parts[2], parts[3], freq)
        if int(parts[1]) < 9:
            continue
        if len(t) >= 2:
            t.pop(0)
            s.pop(0)
        t.append(int(round(time.time() * 1000)))
        s.append(freq)
        ax.plot(t, s)
        plt.pause(0.1)
