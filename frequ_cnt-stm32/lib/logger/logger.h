#ifndef _LOGGER_LOGGER_H_
#define _LOGGER_LOGGER_H_

extern "C"
{
  int semihost_write(const unsigned char *buffer, unsigned int length);
}

void debug_print(const char *s);

#endif
