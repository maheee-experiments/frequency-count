<?php
date_default_timezone_set("Europe/Vienna");
//error_reporting(E_ERROR);
include "./lib/cache-1.0.1/src/CacheException.php";
include "./lib/cache-1.0.1/src/CacheItemInterface.php";
include "./lib/cache-1.0.1/src/CacheItemPoolInterface.php";
include "./lib/cache-1.0.1/src/InvalidArgumentException.php";
include "./lib/stash-0.15.1/autoload.php";
include "./db/db.inc.php";
include "./db/queries.inc.php";
include "./db/data.inc.php";
include "./graph/graph_draw.inc.php";
include "./graph/graph_data.inc.php";

$mysqli = connectDb();

$driver = new Stash\Driver\FileSystem(array('path' => './_cache/'));
$pool = new Stash\Pool($driver);

$todayMode = isset($_GET["h"]);
$pastMode = isset($_GET["d"]);
$statMode = isset($_GET["s"]);
$methMode = isset($_GET["m"]);
$dashMode = !$todayMode && !$pastMode && !$statMode;
/*
$pool->clear();
$pool->purge();
die();
*/
?>
<!DOCTYPE HTML>
<html lang="en">
  <?php include "./parts/head.inc.php"; ?>
  <body>
    <?php
      include "./parts/navbar.inc.php";

      if ($todayMode) {
        include "./parts/today.inc.php";
      } elseif ($pastMode) {
        include "./parts/past.inc.php";
      } elseif ($statMode) {
        include "./parts/stat.inc.php";
      } elseif ($methMode) {
        include "./parts/method.inc.php";
      } else { // dashMode
        include "./parts/dash.inc.php";
      }
    ?>
  </body>
</html>

<?php
disconnectDb($mysqli);
?>
